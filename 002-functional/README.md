# Fill in the Blank: Functions Acquired!

Welcome to another session of "Fill in the Blank"! You'll be
implementing a few common functions this time around. You'll also
start getting used to cabal.

Follow the steps below to get started.

## Getting Started

* Create a sandbox in the folder containing this README

```
$ cabal sandbox init
```

* Install dependencies

```
$ cabal install --dependencies-only --enable-test
```

* Open `src/FunctionsAcquired.hs` in your favorite text editor
* Edit away until you can compile that file
* Run tests:

```
$ cabal test unit
$ cabal test properties
```

* Have fun!
