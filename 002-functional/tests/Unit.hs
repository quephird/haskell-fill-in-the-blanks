{-

Welcome to the unit test module for: Functions Acquired!

We'll be using the Tasty test framework along with an HUnit extension
for Tasty that allows us to write unit-style tests.

Read below to see what tests are like in this style. You're also
welcome to extend the test suite below to ensure your implementation
is correct.

To run in summary mode:

$ cabal test unit

To run verbosely and colorfully, build the tests once and then:

$ ./dist/build/unit/unit

Have fun!

-}
module Main where

import Prelude hiding (
  map, filter, foldr, foldl, sum, product, and, or, any, all, (.),
  reverse, take, drop, zip
  )


import Test.Tasty
import Test.Tasty.HUnit

import FunctionsAcquired

----------------------------------------------------------------------
                      -- Test Infrastructure --
                         -- Bonuses Below --
----------------------------------------------------------------------

-- | Unit tests; good for exploring the domain
-- These get verbose quickly, though!
-- It's often better to tinker with the REPL until
-- the properties of the functions in question start to show through

-- For system properties ("integration" tests), spec-tests are usually
-- more suitable

test :: TestTree
test = testGroup "Unit tests" [
  testGroup "map"
    [ tC "Empty list" $ map (+1) [] @?= []
    , tC "List with one element" $ map (+1) [1] @?= [2]
    , tC "List with multiple elements" $ map (+1) [1..10] @?= [2..11]
    ]

  , testGroup "filter"
    [ tC "Empty list" $ filter (const True) [] @?= ([] :: [Int])
    , tC "Filtering True with one True element" $ filter (const True) [True] @?= [True]
    , tC "Filtering False with one True element" $ filter (const False) [True] @?= ([] :: [Bool])
    , tC "Filtering evens on range from 1 to 10" $ filter even [1..10] @?= [2,4,6,8,10]
    , tC "Filtering odds on range from 1 to 10" $ filter odd [1..10] @?=  [1,3,5,7,9]
    ]

  , testGroup "foldl"
    [ tC "Empty list" $ foldl (+) 0 [] @?= 0
    , tC "Summing one element, starting with 0" $ foldl (+) 0 [42] @?= 42
    , tC "Summing one element, starting with non-zero" $ foldl (+) 21 [21] @?= 42
    , tC "Summing multiple elements, starting with 0" $ foldl (+) 0 [1,2,3,4,5] @?= 15
    , tC "Summing multiple elements, starting with non-zero" $ foldl (+) 21 [1,2,3,4,5,6] @?= 42
    , tC "Subtracting multiple elements, starting with 0" $ foldl (-) 0 [1,2,3,4,5] @?= -15
    ]

  , testGroup "foldr"
    [ tC "Empty list" $ foldr (+) 0 [] @?= 0
    , tC "Summing one element, starting with 0" $ foldr (+) 0 [42] @?= 42
    , tC "Summing one element, starting with non-zero" $ foldr (+) 21 [21] @?= 42
    , tC "Summing multiple elements, starting with 0" $ foldr (+) 0 [1,2,3,4,5] @?= 15
    , tC "Summing multiple elements, starting with non-zero" $ foldr (+) 21 [1,2,3,4,5,6] @?= 42
    , tC "Subtracting multiple elements, starting with 0" $ foldr (-) 0 [1,2,3,4,5] @?= 3
    ]

  , testGroup "sum"
    [ tC "Empty list" $ sum [] @?= 0
    , tC "List with one element" $ sum [42] @?= 42
    , tC "List with multiple elements" $ sum [1,2,3,4,5] @?= 15
    ]

  , testGroup "product"
    [ tC "Empty list" $ product [] @?= 1
    , tC "List with one element" $ product [42] @?= 42
    , tC "List with multiple elements" $ product [1,2,3,7] @?= 42
    ]

  , testGroup "any"
    [ tC "Empty list" $ any even [] @?= False
    , tC "List with no element fulfilling predicate" $ any even [1,3,5,7,9] @?= False
    , tC "List with one element fulfilling predicate" $ any even [1,3,5,7,9,10] @?= True
    ]

  , testGroup "all"
    [ tC "Empty list" $ all even [] @?= True
    , tC "List with no element fulfilling predicate" $ all even [1,3,5,7,9] @?= False
    , tC "List with one element fulfilling predicate" $ all even [1,3,6,7,9] @?= False
    , tC "List with all elements fulfilling predicate" $ all odd [1,3,5,7,9] @?= True
    ]

  , testGroup "reverse"
    [ tC "Empty list" $ reverse [] @?= ([] :: [Int])
    , tC "List with one element" $ reverse [42] @?= [42]
    , tC "List with multiple elements" $ reverse [1,2,3,4,5] @?= [5,4,3,2,1]
    ]

  , testGroup "take"
    [ tC "Taking non-zero from empty list" $ take 5 [] @?= ([] :: [Int])
    , tC "Taking 0 from non-empty list" $ take 0 [1,2,3,4,5] @?= ([] :: [Int])
    , tC "Taking 1 from non-empty list" $ take 1 [1,2,3,4,5] @?= [1]
    , tC "Taking multiple elements from non-empty list" $ take 3 [1,2,3,4,5] @?= [1,2,3]
    , tC "Taking n elements from list with size <n" $ take 10 [1,2,3,4,5] @?= [1,2,3,4,5]
    ]

  , testGroup "drop"
    [ tC "Dropping non-zero from empty list" $ drop 5 [] @?= ([] :: [Int])
    , tC "Dropping 0 from non-empty list" $ drop 0 [1,2,3,4,5] @?= [1,2,3,4,5]
    , tC "Dropping 1 from non-empty list" $ drop 1 [1,2,3,4,5] @?= [2,3,4,5]
    , tC "Dropping multiple elements from non-empty list" $ drop 3 [1,2,3,4,5] @?= [4,5]
    , tC "Dropping n elements from list with size <n" $ drop 10 [1,2,3,4,5] @?= ([] :: [Int])
    ]

  , testGroup "zip"
    [ tC "Two empty lists" $ zip ([] :: [Int]) ([] :: [Int]) @?= ([] :: [(Int,Int)])
    , tC "First list is empty, second is not." $ zip ([] :: [Int]) [6,7,8,9,10] @?= ([] :: [(Int,Int)])
    , tC "Second list is empty, first is not." $ zip [1,2,3,4,5] ([] :: [Int]) @?= ([] :: [(Int,Int)])
    , tC "Both lists non-empty and same size." $ zip [1,2,3,4,5] [6,7,8,9,10] @?= [(1,6),(2,7),(3,8),(4,9),(5,10)]
    , tC "Both lists non-empty, first is larger than second." $ zip [1,2,3] [6,7,8,9,10] @?= [(1,6),(2,7),(3,8)]
    , tC "Both lists non-empty and same size." $ zip [1,2,3,4,5] [8,9,10] @?= [(1,8),(2,9),(3,10)]
    ]

----------------------------------------------------------------------
-- BONUS: Add unit or prop tests until you're sure everything works --
----------------------------------------------------------------------
  , testGroup "bonus" []
  ]
  where tC = testCase

main :: IO ()
main = defaultMain test
