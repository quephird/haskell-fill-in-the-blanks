{-# LANGUAGE ScopedTypeVariables #-}
{-

Welcome to the property test module for: Functions Acquired!

We'll be using the Tasty test framework along with a QuickCheck
extension for Tasty that allows us to write property-style tests.

Property tests are interesting. They're tests where we say, "This must
always be true". QuickCheck then generates several random cases to
check your assumption. If they all pass, great! Your assertion is
very, *very* likely to be true. If any of them fail, the test is
immediately cancelled and the input that cause your assertion to fail
is displayed.

This is a powerful but non-intuitive way to write tests. It takes some
getting used to, and you'll see many algebraic patterns come up time
and again. Read below to get some ideas!

You're welcome to extend the test suite below to ensure your
implementation is correct.

To run in summary mode:

$ cabal test properties

To run verbosely and colorfully, build the tests once and then:

$ ./dist/build/properties/properties

Have fun!

-}
module Main where

import Prelude hiding (
  map, filter, foldr, foldl, sum, product, and, or, any, all, (.),
  reverse, take, drop, zip
  )
import qualified Prelude as P


import Test.Tasty
import Test.Tasty.QuickCheck

import FunctionsAcquired

----------------------------------------------------------------------
                      -- Test Infrastructure --
                  -- Makes For Good Reading, Too --
----------------------------------------------------------------------

----------------------------------------------------------------------
                  -- Oracle-Based Property Tests --
----------------------------------------------------------------------
-- An oracle is something that we trust to always give us the right answer
-- Oracle-based property tests are used in the case where we have an existing
-- implementation that we know is correct. When such a thing is available,
-- this is a really nice way to ensure correctness.
--
-- Realistically speaking, the most likely scenarios where this is
-- going to come up are:
--   * Performance optimization: the oracle is the simpler, slower impl.
--   * Reverse engineering: the oracle is the target protocol
oracleProps :: TestTree
oracleProps = testGroup "Property Tests Using Prelude as Oracle"
  [ testProperty "map with +1" (\(xs :: [Int]) -> map (+1) xs == P.map (+1) xs)
  , testProperty "filter with even" (\(xs :: [Int]) -> filter even xs == P.filter even xs)
  , testProperty "foldl with +" (\(xs :: [Int]) -> foldl (+) 0 xs == P.foldl (+) 0 xs)
  , testProperty "foldr with +" (\(xs :: [Int]) -> foldr (+) 0 xs == P.foldr (+) 0 xs)
  , testProperty "foldr with -" (\(xs :: [Int]) -> foldr (-) 0 xs == P.foldr (-) 0 xs)
  , testProperty "sum" (\(xs :: [Int]) -> sum xs == P.sum xs)
  , testProperty "product" (\(xs :: [Int]) -> product xs == P.product xs)
  , testProperty "any with even" (\(xs :: [Int]) -> any even xs == P.any even xs)
  , testProperty "all with even" (\(xs :: [Int]) -> all even xs == P.all even xs)
  , testProperty "reverse" (\(xs :: [Int]) -> reverse xs == P.reverse xs)
  , testProperty "take" (\(xs :: [Int]) (n :: NonNegative Int) ->
      take (getNonNegative n) xs == P.take (getNonNegative n) xs)
  , testProperty "drop" (\(xs :: [Int]) (n :: NonNegative Int) ->
      drop (getNonNegative n) xs == P.drop (getNonNegative n) xs)
  , testProperty "zip" (\(xs :: [Int]) (ys :: [Int]) -> zip xs ys == P.zip xs ys)
  , testProperty "(.)" (\(n :: Int) m x -> ((+n) . (*m)) x ==  ((+n) P.. (*m)) x)
  ]


----------------------------------------------------------------------
                    -- Algebraic Property Tests --
----------------------------------------------------------------------
-- These aren't always intuitive. Finding algebraic properties for your
-- functions is hard! If you do find them, though, they make for *really* good tests.
lawProps :: TestTree
lawProps = testGroup "Property Tests Using Algebra"
  [ testProperty "zipLeftIdentity" (\(xs :: [Int]) -> zip ([] :: [Int]) xs == [])
  , testProperty "zipRightIdentity" (\(xs :: [Int]) -> zip xs ([] :: [Int]) == [])
  , testProperty "zipStopsShort" (\(xs :: [Int]) (ys :: [Int]) ->
                                   length (zip xs ys) == min (length xs) (length ys)
                                 )

  , testProperty "reverseTwiceIsIdentity" (\(xs :: [Int]) -> (reverse P.. reverse) xs == xs)

  , testProperty "takeNoneEmpty" (\(xs :: [Int]) -> take 0 xs == [])
  , testProperty "takeAllFull" (\(xs :: [Int]) -> take (length xs) xs == xs)

  , testProperty "dropNoneFull" (\(xs :: [Int]) -> drop 0 xs == xs)
  , testProperty "dropAllEmpty" (\(xs :: [Int]) -> drop (length xs) xs == [])

  , testProperty "mapOverEmptyIsEmpty" (map (+1) [] == [])
  , testProperty "mapOverId" (\(xs :: [Int]) -> map id xs == xs)

  , testProperty "filterConstTrueReturnsAll" (\(xs :: [Int]) -> filter (const True) xs == xs)
  , testProperty "filterConstFalseReturnsEmpty" (\(xs :: [Int]) -> filter (const False) xs == [])

  , testProperty "composeAssociative" (\(a :: Int) b c d ->
                                        ( ((+a) . (*b)) . (+c) ) d ==
                                        ( (+a) . ((*b) . (+c)) ) d
                                      )

  , testProperty "sumEmptyListIdentity" (sum [] == 0)

  , testProperty "productEmptyListIdentity" (product [] == 1)

  , testProperty "anyEmptyListIdentity" (any even [] == False)

  , testProperty "allEmptyListIdentity" (all even [] == True)

  , testProperty "foldlCanImplementSum" (\(xs :: [Int]) -> foldl (+) 0 xs == P.sum xs)

  , testProperty "foldrCanImplementSum" (\(xs :: [Int]) -> foldr (+) 0 xs == P.sum xs)
  ]

----------------------------------------------------------------------
    -- BONUS: Add oracle property tests for your TR/fold impls. --
----------------------------------------------------------------------
yourTests = testGroup "Property Tests Using recursive implementations as Oracle"
  [ testProperty "mapTR" (\(xs :: [Int]) -> mapTR (+1) xs == map (+1) xs)
  , testProperty "filterTR" (\(xs :: [Int]) -> filterTR even xs == filter even xs)
  , testProperty "reverseTR" (\(xs :: [Int]) -> reverseTR xs == reverse xs)
  , testProperty "zipTR" (\(xs :: [Int]) (ys :: [Int]) ->
      zipTR xs ys == zip xs ys)
  , testProperty "takeTR" (\(xs :: [Int]) (n :: NonNegative Int) ->
      takeTR (getNonNegative n) xs == take (getNonNegative n) xs)
  , testProperty "dropTR" (\(xs :: [Int]) (n :: NonNegative Int) ->
      dropTR (getNonNegative n) xs == drop (getNonNegative n) xs)
  , testProperty "foldrTR" (\(xs :: [Int]) n -> foldrTR (-) n xs == foldr (-) n xs)
  , testProperty "mapFold" (\(xs :: [Int]) -> mapFold (+1) xs == map (+1) xs)
  , testProperty "filterFold" (\(xs :: [Int]) -> filterFold even xs == filter even xs)
  , testProperty "sumFold" (\(xs :: [Int]) -> sumFold xs == sum xs)
  , testProperty "productFold" (\(xs :: [Int]) -> productFold xs == product xs)
  , testProperty "anyFold" (\(xs :: [Int]) -> anyFold even xs == any even xs)
  , testProperty "allFold" (\(xs :: [Int]) -> allFold even xs == all even xs)
  ]

----------------------------------------------------------------------
                          -- Test Runners --
----------------------------------------------------------------------
test :: TestTree
test = testGroup "Property Tests" [lawProps, oracleProps, yourTests]

main :: IO ()
main = defaultMain test
