# Fill in the Blanks

Welcome to "Fill in the Blanks"! This repository contains a series of
exercises that aim to teach basic Haskell patterns and abstractions.

Feel free to dive into any of the directories to get started.

# Exercise Ordering

The exercises directories are prefixed with codes indicative of
approximate difficulty. In general, X-- will require more
understanding than Y-- to complete, for X > Y. For --X compared to
--Y, the difference will be relatively small.

Here's a tentative key for now:

* 000 level exercises: recursion, language primitives
* 100 level exercises: abstractions - functors, etc.

# Contributing

Be sure to review the included code of conduct. This project adheres
to the [Contributor's Covenant](http://contributor-covenant.org/). By
participating in this project you agree to abide by its terms.

# Licensing

This project is and all associated exercises are distrubted under a
BSD3 license. See the included LICENSE file for more details.
