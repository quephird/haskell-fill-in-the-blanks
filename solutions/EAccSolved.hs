module EAcc where

import Control.Applicative
import Control.Monad
import Data.Monoid

data V l r = E l | R r deriving (Show, Eq)

instance Functor (V l) where
  fmap _ (E l) = E l
  fmap f (R r) = R (f r)

instance Monoid l => Applicative (V l) where
  pure = R
  (E l1) <*> (E l2) = E (l1 <> l2)
  _ <*> (E l) = E l
  (E l) <*> _ = E l
  (R r1) <*> (R r2) = R (r1 r2)

instance Monoid l => Alternative (V l) where
  empty = E mempty
  (E l1) <|> (E l2) = E (l1 <> l2)
  (E _) <|> (R r) = R r
  (R r) <|> _ = R r

instance Monoid l => Monad (V l) where
  return = pure
  (E l) >>= _ = E l
  (R r) >>= m = m r

instance Monoid l => MonadPlus (V l) where
  mzero = empty
  mplus = (<|>)

instance (Monoid r, Monoid l) => Monoid (V l r) where
  mempty = mempty
  mappend (E l1) (E l2) = E (l1 <> l2)
  mappend (E l) _ = E l
  mappend _ (E l) = E l
  mappend (R r1) (R r2) = R (r1 <> r2)

main :: IO ()
main = print [a, b, c, d, e, f, g, h]
  where a = l1 <|> r1
        b = r1 >>= (R . (+1))
        c = (+) <$> r1 <*> r2
        d = (+) <$> l1 <*> r2
        e = (+) <$> r1 <*> l2
        f = (+) <$> l1 <*> l2
        g = r1 <|> r1
        h = l1 <|> l2
        r1 = R 1 :: V [String] Int
        r2 = R 2 :: V [String] Int
        l1 = E ["err1"] :: V [String] Int
        l2 = E ["err2"] :: V [String] Int
