{- |

Fill in the Blank: Functions Acquired!

This exercise asks you to implement several useful functions commonly
used in Haskell programs. This includes:

* map
* filter
* foldr
* foldl
* sum
* product
* any
* all
* (.) - compose
* reverse
* take
* drop
* zip

Everywhere you encounter a '_' is a place to fill in your
solution. These are typed-holes and will give you a compilation error
that's a tiny clue as to what's needed to fill in the blank.

It's okay to do more than fill in the blank. For example, you may find
it helpful to expand the cases to help you:

map f x = _

<expand cases>

map f []     = _
map f (x:xs) = _

Once you've gotten things to compile, run the tests by:

# Only once
$ cabal install --dependencies-only --enable-test

# Every time after
$ cabal test unit        -- unit tests
$ cabal test properties  -- property tests

If you'd like to see more verbose test output:

$ ./dist/build/unit/unit
$ ./dist/build/properties/properties

Good luck!

-}

----------------------------------------------------------------------
                    -- Imports: No Blanks Here --
----------------------------------------------------------------------
module FunctionsAcquired where

import Prelude hiding (
  map, filter, foldr, foldl, sum, product, any, all, (.),
  reverse, take, drop, zip
  )

----------------------------------------------------------------------
              -- Functions Below: Fill in the Blanks --
----------------------------------------------------------------------
map :: (a -> b) -> [a] -> [b]
map _ [] = []
map f (x:xs) = f x : map f xs

filter :: (a -> Bool) -> [a] -> [a]
filter _ [] = []
filter p (x:xs) = if p x then x : filter p xs else filter p xs

foldr :: (a -> b -> b) -> b -> [a] -> b
foldr _ e [] = e
foldr f e (x:xs) = f x (foldr f e xs)

foldl :: (b -> a -> b) -> b -> [a] -> b
foldl _ e [] = e
foldl f e (x:xs) = f (foldl f e xs) x

sum :: Integral a => [a] -> a
sum = foldr (+) 0

product :: Integral a => [a] -> a
product = foldr (*) 1

any :: (a -> Bool) -> [a] -> Bool
any p = foldr (\x xs -> p x || xs) False

all :: (a -> Bool) -> [a] -> Bool
all p = foldr (\x xs -> p x && xs) True

reverse :: [a] -> [a]
reverse [] = []
reverse (x:xs) = reverse xs ++ [x]

take :: Int -> [a] -> [a]
take _ [] = []
take n (x:xs)
  | n > 0 = x : take (n-1) xs
  | otherwise = []

drop :: Int -> [a] -> [a]
drop _ [] = []
drop n (x:xs)
  | n > 0 = drop (n-1) xs
  | otherwise = (x:xs)

zip :: [a] -> [b] -> [(a,b)]
zip (x:xs) (y:ys) = (x,y) : zip xs ys
zip [] _ = []
zip _ [] = []

(.) :: (b -> c) -> (a -> b) -> (a -> c)
f . g = \x -> f (g x)

----------------------------------------------------------------------
        -- BONUS: Fill in the Blanks, Tail Recursion Style --
----------------------------------------------------------------------

{-

A function written in tail-recursive style is one where on every
recursive branch, the function call is the first thing invoked. This
will frequently lead to implementations that are faster. To illustrate
this, let's take a look at a two implementations of map.

map :: (a -> b) -> [a] -> [b]
map _ [] = []
map f (x:xs) = f x : map f xs

This is not tail-recursive. In the recursive branch, map does not come
first. Let's make it tail-recursive!

mapTR :: (a -> b) -> [a] -> [b]
mapTR fn elems = go fn elems []
  where go _ [] acc     = acc
        go f (x:xs) acc = go f xs (f x : acc)

This implementation is tail-recursive. The interface stays the same,
but we define a local function that takes an accumulator parameter and
does all the work. This is a common technique for making a function
tail-recursive.

Feel free to try to fill in the blanks below! Good luck!

-}

mapTR :: (a -> b) -> [a] -> [b]
mapTR f xs = reverse $ go f xs []
  where go _ [] acc = acc
        go f' (x:xs') acc = go f' xs' (f' x : acc)

filterTR :: (a -> Bool) -> [a] -> [a]
filterTR fn elems = reverse $ go fn elems []
  where go _ [] acc = acc
        go p (x:xs) acc = go p xs (if p x then x : acc else acc)

reverseTR :: [a] -> [a]
reverseTR elems = go elems []
  where go [] acc = acc
        go (x:xs) acc = go xs (x:acc)

zipTR :: [a] -> [b] -> [(a,b)]
zipTR ls rs = go ls rs []
  where go (x:xs) (y:ys) acc = go xs ys ((x,y):acc)
        go [] _ acc = acc
        go _ [] acc = acc

takeTR :: Int -> [a] -> [a]
takeTR count elems = go count elems []
  where go _ [] acc = acc
        go n (x:xs) acc
          | n >= 0 = go (n-1) xs (x:acc)
          | otherwise = acc

dropTR :: Int -> [a] -> [a]
dropTR  = drop

foldrTR :: (a -> b -> b) -> b -> [a] -> b
foldrTR fn start elems = go fn elems start
  where go _ [] acc = acc
        go f (x:xs) e = go f xs (f x e)

foldlTR :: (b -> a -> b) -> b -> [a] -> b
foldlTR fn start elems = go fn elems start
  where go _ [] acc = acc
        go f (x:xs) e = go f xs (f e x)


----------------------------------------------------------------------
            -- BONUS: Fill in the Blanks, Reusing Folds --
----------------------------------------------------------------------

{-

Surprisingly, several of the functions above can be implemented in
terms of folds. What's the benefit of this? There's two:

* Correctness: a fold will always terminate; avoids recursion bugs

* Performance: if fold is highly optimized, everything written using a
               fold benefits

* Reuse: any work done on improving folds improves everything written
         using folds

There's a methodical approach to arriving at a fold-based
implementation for a function. I won't cover it in detail
here. Instead, I'll give you a freebie and a link.

Consider the sum function:

sum [] = 0
sum (x:xs) = x + sum xs

Notice that the empty case corresponds with the default value for
sum. Notice that the recursive case accumulates by adding a left side
and a right side (left + right). Now consider the type of the foldl
function:

foldl :: (b -> a -> b) -> b -> [a] -> b

With folds, `b` is the type of the final result. The second parameter
to fold takes a `b`: this is the starting value. The third parameters
is the list we're summarizing. The first parameter is the
trickiest. It's a function that takes two arguments and returns a `b`.

So let's go back to `sum`. Here's what it looks like written with a
fold:

sum xs = foldl (+) 0 xs

If we eta-reduce:

sum = foldl (+) 0

If we give names to the parameters pased to (+):

sum = foldl (\acc x -> acc + x)

If we re-write sum tail-recursively:

sumTR xs = go xs 0
  where go [] acc = acc
        go (x:xs) acc = go xs (acc + x)

This is the secret to deriving a fold. For more details, check out
this paper:

A Tutorial on the Universality and Expressiveness of Fold:
  https://www.cs.nott.ac.uk/~gmh/fold.pdf

Good luck!

-}

mapFold :: (a -> b) -> [a] -> [b]
mapFold f = foldr (\x xs -> f x : xs) []

filterFold :: (a -> Bool) -> [a] -> [a]
filterFold p = foldr (\x xs -> if p x then x : xs else xs) []

sumFold :: [Int] -> Int
sumFold = foldl (+) 0

productFold :: [Int] -> Int
productFold = foldl (*) 1

anyFold :: (a -> Bool) -> [a] -> Bool
anyFold p = foldl (\xs x -> p x || xs) False

allFold :: (a -> Bool) -> [a] -> Bool
allFold p = foldl (\xs x -> p x && xs) True
