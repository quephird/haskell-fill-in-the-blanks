{-# LANGUAGE RankNTypes #-}
module Parsely (
  -- | Env management, where Env wraps Headers/Params/... from Request
  EnvMap,
  Headers,
  Params,
  Env,
  defaultEnv,
  mkEnv,
  mkHeaders,
  mkParams,
  params,
  headers,

  -- | Error type
  Error(..),

  -- | Result and Parser core
  Result(..),
  P(..),

  -- | Parser primitives
  (<?>),
  parse,
  parseMaybe,
  parseEither,
  missing,
  malformed,
  freeform,

  -- | Combinator Primitives
  header,
  param,
  headerOpt,
  paramOpt,

  -- | Derived combinators
  int,
  bool,
  float,
  list,
  choice,

  -- | Class to generate parsers given an environment
  FromEnv
) where

import Prelude hiding (foldr)
import Control.Applicative hiding (many)
import Control.Monad
import Data.Char (toLower)
import Data.Foldable (asum)
import Data.Monoid
import qualified Data.Map as M

----------------------------------------------------------------------
                     -- Environment Management --
----------------------------------------------------------------------
type EnvMap a = M.Map String String
data HeaderMap
data ParamMap

newtype Headers = Headers (EnvMap HeaderMap) deriving Show
newtype Params  = Params (EnvMap ParamMap) deriving Show
data Env
  = Env Headers Params deriving Show

defaultEnv :: Env
defaultEnv = Env (Headers M.empty) (Params M.empty)

mkEnv :: [(String, String)] -> [(String, String)] -> Env
mkEnv hs ps = Env (mkHeaders hs) (mkParams ps)

mkHeaders :: [(String, String)] -> Headers
mkHeaders = Headers . M.fromList

mkParams :: [(String, String)] -> Params
mkParams = Params . M.fromList

params :: Env -> EnvMap ParamMap
params (Env _ (Params ps)) = ps

headers :: Env -> EnvMap HeaderMap
headers (Env (Headers hs) _) = hs

----------------------------------------------------------------------
                         -- Error Messages --
----------------------------------------------------------------------
data Loc = Header String | Param String deriving Show
type Reason = String
data Error
  = Missing Loc
  | Malformed Reason String
  | Freeform Reason
  | Or Error Error
  | And Error Error
  | Annotation String
  | Clear

instance Show Error where
  show (Missing l) = "missing " <> show l
  show (Malformed r s) = "could not parse " <> s <> ": " <> r
  show (Freeform r) = r
  show (Or m1 m2) = show m1 <> " | " <> show m2
  show (And a@(Annotation _) m2) = show a <> show m2
  show (And m1 m2) = show m1 <> "\n" <> show m2
  show (Annotation m) = m <> ": "
  show Clear = ""

instance Monoid Error where
  mempty = Clear
  mappend = And

----------------------------------------------------------------------
                       -- Parser Primitives --
----------------------------------------------------------------------

{-
This whole section is where 90% of the magic lives. By magic, I mean:

* Error propagation strategy
* Choosing next branch depending of success/failure of current parse
* Converting parsed types through validation to final types

-}

data Result a
  = Failure Error
  | Success a

instance Show a => Show (Result a) where
  show (Failure e) = show e
  show (Success a) = show a

-- chain error along all continuations to accumulate
newtype P a =
  P { runP :: forall r.
              (Error -> Result r)
           -> (a -> Error -> Result r)
           -> Result r
    }

instance Functor P where
  fmap f (P p) = P $ \kf ks -> p kf (ks . f)

-- Used in parsers of the form: a >>= b, such that
--   * a is a "pure" type: String, Int, etc.
--   * b is a Parser ("parsed") type: P String, P Int
instance Monad P where
  return a = P $ \_ ks -> ks a mempty
  (P m) >>= k = P $ \kf ks ->
    m kf
      (\a e ->
        runP (k a) (\e'    -> kf (e <> e'))
                   ks
      )

  -- allow user propagation of custom error messages
  -- note: this breaks cumulative error-handling
  fail = freeform

-- Used for sequencing parsers to construct final data type
-- for example: Num <$> x <*> y <*> z
--    where: x, y, and z are all parsers yielded some data type
--           compatible with Num
instance Applicative P where
  pure = return
  (P f) <*> (P p) = P $ \kf ks ->
    p (\e   ->
          f (\e'    -> kf (e <> e'))
            (\_  e' -> kf (e <> e'))  -- this was key to error accumulation:
                               -- understanding that I could pass a
                               -- failure continuation on both paths
                               -- coming from a failure continuation
      )
      (\a e ->
          f (\e'    -> kf (e <> e'))
            (\f' e' -> ks (f' a) e')
      )

-- Used to declare alternatives. For example, specifying that data may live in a
-- a header OR a query param, ala, (header "x" e <|> param "x" e)
instance MonadPlus P where
  mzero = P $ \kf _ -> kf Clear
  mplus (P m) (P n) = P $ \kf ks ->
    m (\e ->
        n (kf . Or e)  -- propagate alternatives in case of failure
          ks
      ) ks

instance Alternative P where
  empty = mzero
  (<|>) = mplus

-- merging the result of two consecutive parsers
-- I don't have an intuition for this just yet
instance Monoid a => Monoid (P a) where
  mempty = P $ \_ ks -> ks mempty Clear
  mappend = mplus

----------------------------------------------------------------------
                       -- Parser Combinators --
----------------------------------------------------------------------
(<?>) :: P a -> String -> P a
p <?> s = P $ \kf ks ->
  runP p (\e -> kf (Annotation s <> e)) (\a e -> ks a (Annotation s <> e))

parse :: (a -> P b) -> a -> Result b
parse m v = runP (m v) Failure (\a _ -> Success a)

parseMaybe :: (a -> P b) -> a -> Maybe b
parseMaybe m v = case parse m v of
  (Failure _) -> Nothing
  (Success x) -> Just x

parseEither :: (a -> P b) -> a -> Either Error b
parseEither m v = case parse m v of
  (Failure x) -> Left x
  (Success x) -> Right x

missing :: Loc -> P a
missing l = P $ \kf _ -> kf (Missing l)

malformed :: Reason -> String -> P a
malformed r s = P $ \kf _ -> kf (Malformed r s)

freeform :: String -> P a
freeform r = P $ \kf _ -> kf (Freeform r)

----------------------------------------------------------------------
                          -- Combinators --
----------------------------------------------------------------------
required :: String -> Loc -> EnvMap a -> P String
required s l = maybe (missing l) pure . M.lookup s

opt :: String -> EnvMap a -> P (Maybe String)
opt s = pure . M.lookup s

header :: String -> Env -> P String
header s = required s (Header s) . headers

param :: String -> Env -> P String
param s = required s (Param s) . params

headerOpt :: String -> Env -> P (Maybe String)
headerOpt s = opt s . headers

paramOpt :: String -> Env -> P (Maybe String)
paramOpt s = opt s . params

int :: (Read a, Integral a) => String -> P a
int s =
  case reads s of
    [(a,_)] -> pure a
    _       -> malformed "expecting integral number" (show s)

bool :: String -> P Bool
bool s = case lowercase s of
           "true"  -> pure True
           "false" -> pure False
           _       -> malformed "expecting 'true' or 'false'" s

float :: (Read a, Fractional a) => String -> P a
float s =
  case reads s of
    [(a,_)] -> pure a
    _       -> malformed "expecting floating point number" (show s)

-- |
-- list :: String -> Char -> P [String], for example
-- > newtype Scope = Scope [String] deriving Show
-- > fromEnv e = Scope <$> param "scope" e >>= (list ',') -- as implementation for fromEnv
-- > parse fromEnv (Env (mkHeaders []) (mkParams [("scope", "user,edit")])) :: Result Scope
-- Scope ["user", "edit"]
list :: (Eq a) => [a] -> a -> P [[a]]
list s delim = pure $ go s delim
  where go [] _ = []
        go s' d = a : go s'' d
          where (a,s'') = break (== delim) s'

choice :: [P a] -> P a
choice = asum

lowercase :: String -> String
lowercase = map toLower

----------------------------------------------------------------------
                        -- Class Machinery --
----------------------------------------------------------------------
class FromEnv a where
  fromEnv :: Env -> P a

----------------------------------------------------------------------
                            -- Example --
----------------------------------------------------------------------

-- "extra" data "validation"
bigInt :: Int -> P Int
bigInt n
  | n > 10    = pure n
  | otherwise = malformed "must be > 10" (show n)

data Num' = Num' Int Int Int deriving Show

instance FromEnv Num' where
  fromEnv e = Num' <$> xnum <*> ynum <*> znum
    where xnum = (intParam "x" e <|> intParam "xx" e <|> intParam "xxx" e) <?> "Param x"
          ynum = (intParam "y" e <|> intParam "yy" e) <?> "Param y"
          znum = (intParam "z" e <|> intParam "zz" e) <?> "Param z"
          intParam s e' = param s e' >>= int >>= bigInt

{-| Session
*Parsely> parse fromEnv (Env (mkHeaders []) (mkParams [("x", "13"), ("z", "14"), ("y", "15")])) :: Result Num'
Num' 13 15 14
*Parsely> parse fromEnv (Env (mkHeaders []) (mkParams [("xx", "130"), ("z", "14"), ("y", "15")])) :: Result Num'
Num' 130 15 14
*Parsely> parse fromEnv (Env (mkHeaders []) (mkParams [("x", "10"), ("z", "14"), ("y", "15")])) :: Result Num'
Param z:
Param y:
Param x:
could not parse 10: must be > 10 | missing Param "xx" | missing Param "xxx"
*Parsely> parse fromEnv (Env (mkHeaders []) (mkParams [("", "10"), ("", "14"), ("", "15")])) :: Result Num'
Param z: missing Param "z" | missing Param "zz"
Param y: missing Param "y" | missing Param "yy"
Param x: missing Param "x" | missing Param "xx" | missing Param "xxx"
-}

----------------------------------------------------------------------
                  -- Example: Expression Request --
----------------------------------------------------------------------

data Op = Plus | Minus | Mult | Div deriving Show
data Expr
  = Lit Int
  | Operation Op Expr Expr
    deriving Show

op :: String -> P Op
op o = case o of
  "+" -> pure Plus
  "-" -> pure Minus
  "*" -> pure Mult
  "/" -> pure Div
  _   -> malformed "operation must be one of (+,-,/,*), found" (show o)

instance FromEnv Expr where
  fromEnv e = (Operation <$> op' <*> (Lit <$> num' "x") <*> (Lit <$> num' "y")) <|> (Lit <$> num' "x")
    where op' = param "op" e >>= op
          num' s = param s e >>= int

{-| Session
*Parsely> parse fromEnv (Env (mkHeaders []) (mkParams [("x", "12"), ("op", "+"), ("", "10")])) :: Result Expr
Lit 12
*Parsely> parse fromEnv (Env (mkHeaders []) (mkParams [("x", "12"), ("op", "+"), ("y", "10")])) :: Result Expr
Operation Plus (Lit 12) (Lit 10)
*Parsely> parse fromEnv (Env (mkHeaders []) (mkParams [("", "12"), ("op", "+"), ("y", "10")])) :: Result Expr

missing Param "x"
 | missing Param "x"
-}
