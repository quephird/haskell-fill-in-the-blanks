module Mabes where

import Control.Applicative
import Control.Monad
import Data.Monoid

data M a = J a | N deriving (Show, Eq)

instance Functor M where
  fmap _ N = N
  fmap f (J a) = J (f a)

instance Applicative M where
  pure = J
  N <*> _ = N
  _ <*> N = N
  (J a) <*> (J b) = J (a b)

instance Alternative M where
  empty = N
  N <|> N = N
  N <|> (J b) = J b
  (J a) <|> _ = J a

instance Monad M where
  return = pure
  N     >>= _ = N
  (J a) >>= m = m a

instance Monoid a => Monoid (M a) where
  mempty = mempty
  mappend N a = a
  mappend a N = a
  mappend (J a) (J b) = J (mappend a b)

instance MonadPlus M where
  mzero = empty
  mplus = (<|>)

expect :: (Eq a, Show a) => a -> a -> String -> IO ()
expect f exp name =
  if f == exp
  then print $ "pass: " <> name
  else print $ "error: expecting (" <> show exp <> "), found (" <> show f <> ") in test: " <> name

expectInt :: M Int -> M Int -> String -> IO ()
expectInt = expect

expectString :: M String -> M String -> String -> IO ()
expectString = expect

tests :: IO ()
tests = sequence_
  [
    -- testing Functor
    expectInt ((+1) <$> J 1) (J 2) "fmap f J"
  , expectInt ((+1) <$> N) N "fmap f N"

    -- testing Applicative
  , expectInt ((+) <$> J 1 <*> J 2) (J 3) "<*> J J"
  , expectInt ((+) <$> N <*> J 1) N "<*> N J"
  , expectInt ((+) <$> J 1 <*> N) N "<*> J N"
  , expectInt ((+) <$> N <*> N) N "<*> N N"

   -- testing Alternative and MonadPlus
  , expectInt (J 1 <|> J 2) (J 1) "J <|> J"
  , expectInt (J 1 <|> N) (J 1) "J <|> N"
  , expectInt (N <|> J 2) (J 2) "N <|> J"
  , expectInt (N <|> N) N "N <|> N"

    -- testing Monoid
    -- these should probably be property tests:
    --   mempty <> x = x
    --   x <> mempty = x
    --   mempty <> mempty = mempty
  , expectString (J "cat" <> J "dog") (J "catdog") "J <> J"
  , expectString (J "cat" <> N) (J "cat") "J <> N"
  , expectString (N <> J "dog") (J "dog") "N <> J"
  , expectString (N <> N :: M String) (N) "N <> N"

    -- Testing Monad
  , expectInt (J 1 >>= (\a -> J $ a + 1)) (J 2) "J >>= J"
  , expectInt (J 1 >>= (\_ -> N)) N "J >>= N"
  , expectInt (N >>= (\a -> J $ a + 1)) N "N >>= J"
  , expectInt (N >>= (\_ -> N)) N "N >>= N"
  ]

main :: IO ()
main = tests
